#!/bin/python

import healpy as hp
import numpy as np
from scipy.io import loadmat
import h5py
from scipy.special import erfc
import time
import scipy
from scipy.stats import norm
import time
import os

class FoldedData(object):
    '''
    References:
    [1] Goncharov, B., & Thrane, E. (2018). All-sky radiometer for narrowband
    gravitational waves using folded data. Physical Review D, 98(6), 064018.
    [2] Ain, A., Suresh, J., & Mitra, S. (2018). Very fast stochastic gravitational wave
    background map making using folded data. Physical Review D, 98(2), 024001.
    '''
    def __init__(self,folded_data_file=None,dirty_map_file=None,fisher_map_file=None):
        
        self.folded_data_file = folded_data_file
        self.dirty_map_file = dirty_map_file
        self.fisher_map_file = fisher_map_file
        
    # To be added:
        
    #def get_dirty_map(self):
    #    #Runs PyStoch
    
    #def get_rho_map(self):
    #    #Runs PyStoch
    
    # def estimate_ms_fit(self):
        # Get mu_fit(f) and sigma_fit(f) to convert maxSNR(f) to lambda(f)
        # Input: lambda_f from simulations
    
    # def get_lambda_f(self)
    #    #convertion max_{sky}SNR(f) to lambda(f)
        
    def calculate_rho(self):
        ''' Equation 9 from [1], the distribution of rho is Gaussian '''
        frames_preloaded = h5py.File(self.folded_data_file, "r")
        csd = frames_preloaded['csd'][:]
        sigma_sq_inv = frames_preloaded['sigma_sq_inv'][:]
        frames_preloaded.close()
    
        self.rho = csd/np.sqrt(sigma_sq_inv)
        self.rho_std = np.std(np.real(self.rho),axis=0)
        self.rho_mean = np.mean(np.real(self.rho),axis=0)
        
    def get_veto_mask(self,rho_std_high,rho_std_low=0):
        ''' Get a mask array for removing frequency bins from analysis,
        by applying a veto on std(rho) '''
        self.veto_mask = (self.rho_std < rho_std_high) * (self.rho_std > rho_std_low)
        
    def simulate_one_folded_dataset(self,do_veto=False):
        rho_fake = np.random.normal(self.rho_mean,self.rho_std,self.rho.shape).T
        if do_veto:
            rho_fake[self.veto_mask] = None
        return rho_fake
        
    def simulate_and_save_folded_data(self,output_directory,n_sim,do_veto=False):
        
        if not os.path.exists(output_directory):
            os.makedirs(output_directory)
            
        print('Simulating and saving folded rho...')
        for ii in range(n_sim):
            rho_fake = simulate_one_folded_data(do_veto=do_veto)
            
            hf = h5py.File(output_directory + '/sim_'+str(ii)+'.hdf5', 'w')
            hf.create_dataset('rho', data=rho_fake)
            hf.close()
            
            print(ii+1,'/',n_sim)

def parse_commandline():
  """@parse the options given on the command-line.
  """
  parser = optparse.OptionParser()

  parser.add_option("-n", "--num", help="Simulation number",  default=0, type=int)

  opts, args = parser.parse_args()
