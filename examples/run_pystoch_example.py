from PyStoch import PyStoch
import postproc_code
import numpy as np

test_obj = PyStoch("param_general.ini")

test_obj_post = postproc_code.FoldedData( \
                           folded_data_file = test_obj.frame_data_name)
test_obj_post.calculate_rho()
for ii in range(1):
  rho_fake_ft = test_obj_post.simulate_one_folded_dataset()
  rho_fake_sky = test_obj.calculate_maps_general(rho_fake_ft.T)
  max_rho = np.max(rho_fake_sky,axis=1).shape

test_obj.get_sky_maps_from_ft()
