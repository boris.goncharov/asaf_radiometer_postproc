import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="asaf_postproc",
    version="0.0.1",
    author="Boris Goncharov, Eric Thrane",
    author_email="goncharov.boris@physics.msu.ru",
    description="All-sky all-frequency radiometer post-processing code",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://git.ligo.org/boris.goncharov/asaf_radiometer_postproc",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 2.7",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
