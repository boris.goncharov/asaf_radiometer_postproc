# asaf_radiometer_postproc

Piython-based post-processing code for all-sky all-frequency radiometer
 - background estimation
 - detection statistics

Installation:
> python2 setup.py install --user

Method paper:
```
@article{goncharov2018all,
  title={All-sky radiometer for narrowband gravitational waves using folded data},
  author={Goncharov, Boris and Thrane, Eric},
  journal={Physical Review D},
  volume={98},
  number={6},
  pages={064018},
  year={2018},
  publisher={APS}
}
```
